#!/usr/bin/env ruby
# frozen_string_literal: true

require 'kramdown'

abort 'No directory provided' if ARGV[0].nil?
base = File.expand_path(ARGV[0])

section    = ''
subsection = ''
report     = ''

Dir["#{base}/**/*.md"].sort.each do |file|
  file =~ %r{#{base}\/(.+)\/(.+)\/(.+)\.md}
  unless Regexp.last_match(1) == section
    report += "# #{Regexp.last_match(1)}\n\n"
    section = Regexp.last_match(1)
  end
  unless Regexp.last_match(2) == subsection
    report += "## #{Regexp.last_match(2)}\n\n"
    subsection = Regexp.last_match(2)
  end
  report += "### #{Regexp.last_match(3)}\n\n"
  report += File.read(file)
  report += "\n\n"
end

pdf = Kramdown::Document.new(report).to_pdf
File.write("#{base}/report.pdf", pdf)

puts "Generated #{base}/report.pdf"
