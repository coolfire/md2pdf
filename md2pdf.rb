#!/usr/bin/env ruby
# frozen_string_literal: true

require 'kramdown'

input = File.read(ARGV[0])

puts Kramdown::Document.new(input).to_pdf
